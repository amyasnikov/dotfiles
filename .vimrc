" vim config without plugins
set nocompatible
set number
set relativenumber
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set ignorecase
set smartindent
set hls
set is
set showcmd
" set mouse=a
set mousehide
set termencoding=utf-8
set fileencodings=utf-8
set encoding=utf-8
set wrap
set linebreak
set nolist
set autoindent
filetype plugin indent on
syntax enable
set cursorline
hi CursorLine term=bold cterm=bold ctermbg=16 guibg=Grey40
hi CursorLineNR cterm=bold ctermbg=16 ctermfg=9
set clipboard=

set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz
set langmap=ёйцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;$;\\,.pyfgcrl/@aoeuidhtns-'qjkxbmwv:<>PYFGCRL?^AOEUIDHTNS_\\"QJKXBMWV

let g:netrw_banner=0
let g:netrw_liststyle=3
let g:netrw_browse_split=3
let g:netrw_preview=1
let g:netrw_keepdir=0
let g:netrw_winsize=30
let g:netrw_hide=0


return {
	{
		"folke/trouble.nvim",
		-- opts will be merged with the parent spec
		opts = { use_diagnostic_signs = true },
	},

	{
		"nvim-treesitter/nvim-treesitter",
		opts = {
			ensure_installed = {
				"bash",
				"html",
				"javascript",
				"json",
				"lua",
				"markdown",
				"markdown_inline",
				"python",
				"query",
				"regex",
				"tsx",
				"typescript",
				"vim",
				"yaml",

				"go",
				"gomod",
				"gosum",
				"gowork",
				"c",
				"cpp",
				"dockerfile",
				"sql",
				"nasm",
				"cmake",
			},
		},
	},

	{
		"williamboman/mason.nvim",
		opts = {
			ensure_installed = {
				"gopls",
				"golangci-lint",
				"python-lsp-server",
			},
		},
	},

	{
		"Exafunction/codeium.nvim",
		opts = {
			enable_cmp_source = vim.g.ai_cmp,
			virtual_text = {
				enabled = vim.g.ai_cmp, -- enabled = not vim.g.ai_cmp,
				key_bindings = {
					accept = false, -- handled by nvim-cmp / blink.cmp
					next = "<C-n>",
					prev = "",
				},
			},
		},
	},

	{
		"nvim-neo-tree/neo-tree.nvim",
		opts = {
			filesystem = {
				filtered_items = {
					visible = true,
					hide_dotfiles = false,
					hide_gitignored = false,
				},
			},
		},
	},
}

-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

vim.g.ai_cmp = true

vim.opt.clipboard = ""
vim.opt.colorcolumn = "120"
vim.opt.langmap =
	[[й',ц\,,у.,кp,еy,нf,гg,шc,щr,зl,х/,ъ=,фa,ыo,вe,аu,пi,рd,оh,лt,дn,жs,э-,я\;,чq,сj,мk,иx,тb,ьm,бw,юv,Й",Ц<,У>,КP,ЕY,НF,ГG,ШC,ЩR,ЗL,Х?,Ъ+,ФA,ЫO,ВE,АU,ПI,РD,ОH,ЛT,,ДN,ЖS,Э_,Я:,ЧQ,СJ,МK,ИX,ТB,ЬM,БW,ЮV,ё`,Ë~]]

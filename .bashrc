if [[ ! -v BASHRC_AMYASNIKOV ]]; then
        BASHRC_AMYASNIKOV=Y
else
        return 0
fi

source $HOMEBREW_PREFIX/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOMEBREW_PREFIX/share/zsh-autocomplete/zsh-autocomplete.plugin.zsh

export FZF_DEFAULT_OPTS='--height=90% --preview="echo {} | strings" --preview-window=down:30%:wrap'
export FZF_CTRL_T_COMMAND="rg --files --smart-case --hidden --follow 2> /dev/null"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export PATH=$PATH:$(go env GOPATH)/bin
export PATH=$PATH:$HOME/.config/emacs/bin

export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
export PATH="/opt/homebrew/opt/sqlite/bin:$PATH"





alias vim='nvim'
export EDITOR='nvim'



export PROMPT_COMMAND='history -a'
export LC_ALL="en_US.UTF-8"

# https://github.com/otiai10/gosseract/issues/234#issuecomment-1248991263
export CPATH="/opt/homebrew/include"
export LIBRARY_PATH="/opt/homebrew/lib"

# https://www.wechall.net/warboxes
export WECHALLUSER="amyasnikov"
export WECHALLTOKEN="CA056-1A44D-6B195-4566D-82DE8-872D1"

export GOPRIVATE=*yadro*



alias gtask='task --taskfile $HOME/.taskfiles/Taskfile.yml'

